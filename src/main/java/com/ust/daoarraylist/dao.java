package com.ust.daoarraylist;

import java.util.List;
import com.ust.modelarraylist.ItemType;
public interface dao {
	
	boolean insertItemDetail(ItemType itm);
	List<ItemType> getAllItemDetails();

}
