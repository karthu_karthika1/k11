package com.ust.dao;
import java.util.Map;

import com.ust.model.ClientInfo;
public interface Clientdao {
	
	boolean insertClientDetails(String 	passportNumber,ClientInfo client);	
	Map<String, ClientInfo> getAllClientDetails();

}

